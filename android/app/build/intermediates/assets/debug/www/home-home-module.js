(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]
    }
];
var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.html":
/*!*************************************!*\
  !*** ./src/app/home/home.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n        <ion-row class=\"home-header\">\n            <ion-col size=\"1\" (click)=\"toggleMenu()\"><img src=\"assets/images/menu.png\" width=\"80%\"\n                                                          alt=\"placeholder+image\"></ion-col>\n            <ion-col size=\"10\"><img src=\"assets/images/top_logo.png\" width=\"\" height=\"30px\" alt=\"placeholder+image\">\n            </ion-col>\n            <ion-col size=\"1\"><img src=\"assets/images/messages.png\" width=\"80%\" alt=\"placeholder+image\"></ion-col>\n        </ion-row>\n        <!--          <ion-title>-->\n        <!--            Ionic Blank-->\n        <!--          </ion-title>-->\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n    <ion-row class=\"home-block\" [routerLink]=\"['/expenses']\">\n        <ion-col size=\"3\">\n            <img src=\"assets/images/expenses.png\" width=\"60%\" alt=\"placeholder+image\">\n        </ion-col>\n        <ion-col size=\"9\">\n            <h4>Expenses</h4>\n        </ion-col>\n    </ion-row>\n\n    <ion-row class=\"home-block\" [routerLink]=\"['/event_summary']\">\n        <ion-col size=\"3\">\n            <img src=\"assets/images/event_summery.png\" width=\"60%\" alt=\"placeholder+image\">\n        </ion-col>\n        <ion-col size=\"9\">\n            <h4>Event Summery</h4>\n        </ion-col>\n    </ion-row>\n\n    <ion-row class=\"home-block\" [routerLink]=\"['/members']\">\n\n        <ion-col size=\"3\">\n            <img src=\"assets/images/members.png\" width=\"60%\" alt=\"placeholder+image\">\n        </ion-col>\n\n        <ion-col size=\"9\">\n            <h4>Members</h4>\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row class=\"home-block\" [routerLink]=\"['/messages']\">\n        <ion-col size=\"3\">\n            <img src=\"assets/images/chat.png\" width=\"60%\" alt=\"placeholder+image\">\n        </ion-col>\n        <ion-col size=\"9\">\n            <h4>Chat</h4>\n        </ion-col>\n    </ion-row>\n\n    <ion-row class=\"home-block\" [routerLink]=\"['/payback-facility']\">\n        <ion-col size=\"3\">\n            <img src=\"assets/images/pay_back.png\" width=\"60%\" alt=\"placeholder+image\">\n        </ion-col>\n        <ion-col size=\"9\">\n            <h4>Payback Facility</h4>\n        </ion-col>\n    </ion-row>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = /** @class */ (function () {
    function HomePage(menu) {
        this.menu = menu;
    }
    HomePage.prototype.ngOnInit = function () {
        this.menu.enable(true, 'first');
    };
    HomePage.prototype.toggleMenu = function () {
        if (!this.isMenuOpened) {
            this.menu.open('first').catch(function (error) { return console.log(error); });
        }
        else {
            this.menu.close('first').catch(function (error) { return console.log(error); });
        }
    };
    HomePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.page.html */ "./src/app/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")],
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["MenuController"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map