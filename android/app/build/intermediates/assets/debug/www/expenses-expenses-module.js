(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["expenses-expenses-module"],{

/***/ "./src/app/expenses/expenses.module.ts":
/*!*********************************************!*\
  !*** ./src/app/expenses/expenses.module.ts ***!
  \*********************************************/
/*! exports provided: ExpensesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpensesPageModule", function() { return ExpensesPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _expenses_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./expenses.page */ "./src/app/expenses/expenses.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _expenses_page__WEBPACK_IMPORTED_MODULE_5__["ExpensesPage"]
    }
];
var ExpensesPageModule = /** @class */ (function () {
    function ExpensesPageModule() {
    }
    ExpensesPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_expenses_page__WEBPACK_IMPORTED_MODULE_5__["ExpensesPage"]]
        })
    ], ExpensesPageModule);
    return ExpensesPageModule;
}());



/***/ }),

/***/ "./src/app/expenses/expenses.page.html":
/*!*********************************************!*\
  !*** ./src/app/expenses/expenses.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n        <ion-buttons slot=\"start\">\n            <ion-back-button color=\"energized\"></ion-back-button>\n        </ion-buttons>\n        <ion-title>\n            <h2>Expenses</h2>\n        </ion-title>\n        <!--        <ion-row>-->\n        <!--            <ion-col size=\"12\">-->\n        <!--                <h2>Expenses</h2>-->\n        <!--            </ion-col>-->\n        <!--        </ion-row>-->\n    </ion-toolbar>\n\n</ion-header>\n\n<ion-content>\n\n    <ion-grid>\n\n\n        <ion-row>\n            <ion-col size=\"12\">\n                <ion-searchbar></ion-searchbar>\n            </ion-col>\n        </ion-row>\n\n\n        <ion-row class=\"members-block\">\n            <ion-col size=\"9\">\n                <h5>Name of Expenses</h5>\n                <p>Lorem Ipsum is simply dummy text</p>\n            </ion-col>\n            <ion-col size=\"3\">\n                <span style=\"color: red\">You owe <br/> <strong>SGD 9241</strong></span>\n            </ion-col>\n        </ion-row>\n\n        <ion-row class=\"members-block\">\n            <ion-col size=\"9\">\n                <h5>Name of Expenses</h5>\n                <p>Lorem Ipsum is simply dummy text</p>\n            </ion-col>\n            <ion-col size=\"3\">\n                <span style=\"color: #129546\">Owes you<br/> <strong>SGD 2300</strong></span>\n            </ion-col>\n        </ion-row>\n\n        <ion-row class=\"members-block\">\n            <ion-col size=\"9\">\n                <h5>Name of Expenses</h5>\n                <p>Lorem Ipsum is simply dummy text</p>\n            </ion-col>\n            <ion-col size=\"3\">\n                <span style=\"padding-top: 10px;\">Settled up</span>\n            </ion-col>\n        </ion-row>\n\n        <ion-row class=\"members-block\">\n            <ion-col size=\"9\">\n                <h5>Name of Expenses</h5>\n                <p>Lorem Ipsum is simply dummy text</p>\n            </ion-col>\n            <ion-col size=\"3\">\n                <span style=\"padding-top: 10px;\">Settled up</span>\n            </ion-col>\n        </ion-row>\n\n        <ion-row class=\"members-block\">\n            <ion-col size=\"9\">\n                <h5>Name of Expenses</h5>\n                <p>Lorem Ipsum is simply dummy text</p>\n            </ion-col>\n            <ion-col size=\"3\">\n                <span style=\"color: #129546\">Owes you<br/> <strong>SGD 2300</strong></span>\n            </ion-col>\n        </ion-row>\n\n        <ion-row class=\"members-block\">\n            <ion-col size=\"9\">\n                <h5>Name of Expenses</h5>\n                <p>Lorem Ipsum is simply dummy text</p>\n            </ion-col>\n            <ion-col size=\"3\">\n                <span style=\"color: red\">You owe <br/> <strong>SGD 9241</strong></span>\n            </ion-col>\n        </ion-row>\n\n\n    </ion-grid>\n\n</ion-content>\n  \n"

/***/ }),

/***/ "./src/app/expenses/expenses.page.scss":
/*!*********************************************!*\
  !*** ./src/app/expenses/expenses.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V4cGVuc2VzL2V4cGVuc2VzLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/expenses/expenses.page.ts":
/*!*******************************************!*\
  !*** ./src/app/expenses/expenses.page.ts ***!
  \*******************************************/
/*! exports provided: ExpensesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpensesPage", function() { return ExpensesPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ExpensesPage = /** @class */ (function () {
    function ExpensesPage() {
    }
    ExpensesPage.prototype.ngOnInit = function () {
    };
    ExpensesPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-expenses',
            template: __webpack_require__(/*! ./expenses.page.html */ "./src/app/expenses/expenses.page.html"),
            styles: [__webpack_require__(/*! ./expenses.page.scss */ "./src/app/expenses/expenses.page.scss")],
        }),
        __metadata("design:paramtypes", [])
    ], ExpensesPage);
    return ExpensesPage;
}());



/***/ })

}]);
//# sourceMappingURL=expenses-expenses-module.js.map