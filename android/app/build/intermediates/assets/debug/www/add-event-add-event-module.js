(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-event-add-event-module"],{

/***/ "./src/app/add-event/add-event.module.ts":
/*!***********************************************!*\
  !*** ./src/app/add-event/add-event.module.ts ***!
  \***********************************************/
/*! exports provided: AddEventPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddEventPageModule", function() { return AddEventPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _add_event_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-event.page */ "./src/app/add-event/add-event.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _add_event_page__WEBPACK_IMPORTED_MODULE_5__["AddEventPage"]
    }
];
var AddEventPageModule = /** @class */ (function () {
    function AddEventPageModule() {
    }
    AddEventPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"]
            ],
            declarations: [_add_event_page__WEBPACK_IMPORTED_MODULE_5__["AddEventPage"]]
        })
    ], AddEventPageModule);
    return AddEventPageModule;
}());



/***/ }),

/***/ "./src/app/add-event/add-event.page.html":
/*!***********************************************!*\
  !*** ./src/app/add-event/add-event.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n        <ion-buttons slot=\"start\">\n            <ion-back-button color=\"energized\"></ion-back-button>\n        </ion-buttons>\n        <ion-title>\n            <h2>Add Event</h2>\n        </ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n    <ion-grid>\n        <form [formGroup]=\"eventForm\" (ngSubmit)=\"onSubmit()\" #eventFormDir=\"ngForm\">\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"10\" class=\"login-block\">\n                    Event name\n                    <input autocapitalize=\"none\" type=\"text\" formControlName=\"name\"/>\n                    <div *ngIf=\"submitted && eventForm.controls.name.errors\" class=\"error\">\n                        <div style=\"color: red;  margin-top: -20px;  position: absolute;\"\n                             *ngIf=\"eventForm.controls.name.errors.required\">Name is required\n                        </div>\n                    </div>\n                </ion-col>\n                <ion-col></ion-col>\n            </ion-row>\n\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"10\" class=\"login-block\">\n                    Event Description\n                    <input autocapitalize=\"none\" type=\"text\" formControlName=\"description\"/>\n                    <div *ngIf=\"submitted && eventForm.controls.description.errors\" class=\"error\">\n                        <div style=\"color: red;  margin-top: -20px;  position: absolute;\"\n                             *ngIf=\"eventForm.controls.description.errors.required\">Description is required\n                        </div>\n                    </div>\n                </ion-col>\n                <ion-col></ion-col>\n            </ion-row>\n\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"10\" class=\"login-block\">\n                    Event T&C\n                    <input autocapitalize=\"none\" type=\"text\" formControlName=\"tnc\"/>\n                    <div *ngIf=\"submitted && eventForm.controls.tnc.errors\" class=\"error\">\n                        <div style=\"color: red;  margin-top: -20px;  position: absolute;\"\n                             *ngIf=\"eventForm.controls.tnc.errors.required\">Tnc is required\n                        </div>\n                    </div>\n                </ion-col>\n                <ion-col></ion-col>\n            </ion-row>\n\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"10\" class=\"login-block\">\n                    Event Valid Days\n                    <input autocapitalize=\"none\" type=\"text\" formControlName=\"validDays\"/>\n                    <div *ngIf=\"submitted && eventForm.controls.validDays.errors\" class=\"error\">\n                        <div style=\"color: red;  margin-top: -20px;  position: absolute;\"\n                             *ngIf=\"eventForm.controls.validDays.errors.required\">Valid Days is required\n                        </div>\n                    </div>\n                </ion-col>\n                <ion-col></ion-col>\n            </ion-row>\n\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"10\" class=\"login-block\">\n                    Event Password\n                    <input autocapitalize=\"none\" type=\"text\" formControlName=\"password\"/>\n                    <div *ngIf=\"submitted && eventForm.controls.password.errors\" class=\"error\">\n                        <div style=\"color: red;  margin-top: -20px;  position: absolute;\"\n                             *ngIf=\"eventForm.controls.password.errors.required\">Password is required\n                        </div>\n                    </div>\n                </ion-col>\n                <ion-col></ion-col>\n            </ion-row>\n\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"10\" class=\"login-block\">\n                    <button ion-button\n                            full\n                            color=\"#fd6700\"\n                    >\n                        Add Event\n                    </button>\n                </ion-col>\n                <ion-col></ion-col>\n            </ion-row>\n        </form>\n    </ion-grid>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/add-event/add-event.page.scss":
/*!***********************************************!*\
  !*** ./src/app/add-event/add-event.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkZC1ldmVudC9hZGQtZXZlbnQucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/add-event/add-event.page.ts":
/*!*********************************************!*\
  !*** ./src/app/add-event/add-event.page.ts ***!
  \*********************************************/
/*! exports provided: AddEventPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddEventPage", function() { return AddEventPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _models_Event__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/Event */ "./src/app/models/Event.ts");
/* harmony import */ var _services_event_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var AddEventPage = /** @class */ (function () {
    function AddEventPage(formBuilder, eventService, loadingController, navCtrl) {
        this.formBuilder = formBuilder;
        this.eventService = eventService;
        this.loadingController = loadingController;
        this.navCtrl = navCtrl;
        this.eventForm = this.formBuilder.group({
            name: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            description: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            tnc: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            validDays: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
        });
    }
    AddEventPage.prototype.ngOnInit = function () {
    };
    AddEventPage.prototype.onSubmit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var event, loading;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.submitted = true;
                        if (this.eventForm.invalid) {
                            return [2 /*return*/];
                        }
                        event = new _models_Event__WEBPACK_IMPORTED_MODULE_2__["Event"]();
                        event.name = this.eventForm.value.name;
                        event.description = this.eventForm.value.description;
                        event.password = this.eventForm.value.password;
                        event.tnc = this.eventForm.value.tnc;
                        event.validDays = this.eventForm.value.validDays;
                        return [4 /*yield*/, this.loadingController.create({
                                spinner: 'circles',
                                message: 'Please wait...'
                            })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.eventService.createEvent(event).then(function (response) {
                            alert('Event successfully created');
                            _this.navCtrl.goBack();
                        }, function (error) { return console.log('error', JSON.stringify(error)); }).finally(function () { return loading.dismiss(); });
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Present loading screen.
     * @param loading
     */
    AddEventPage.prototype.presentLoading = function (loading) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AddEventPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-event',
            template: __webpack_require__(/*! ./add-event.page.html */ "./src/app/add-event/add-event.page.html"),
            styles: [__webpack_require__(/*! ./add-event.page.scss */ "./src/app/add-event/add-event.page.scss")],
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _services_event_service__WEBPACK_IMPORTED_MODULE_3__["EventService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]])
    ], AddEventPage);
    return AddEventPage;
}());



/***/ })

}]);
//# sourceMappingURL=add-event-add-event-module.js.map