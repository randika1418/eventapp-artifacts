(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["register-register-module"],{

/***/ "./src/app/_helpers/must-match.validator.ts":
/*!**************************************************!*\
  !*** ./src/app/_helpers/must-match.validator.ts ***!
  \**************************************************/
/*! exports provided: MustMatch */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MustMatch", function() { return MustMatch; });
// custom validator to check that two fields match
function MustMatch(controlName, matchingControlName) {
    return function (formGroup) {
        var control = formGroup.controls[controlName];
        var matchingControl = formGroup.controls[matchingControlName];
        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }
        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        }
        else {
            matchingControl.setErrors(null);
        }
    };
}


/***/ }),

/***/ "./src/app/register/register.module.ts":
/*!*********************************************!*\
  !*** ./src/app/register/register.module.ts ***!
  \*********************************************/
/*! exports provided: RegisterPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function() { return RegisterPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _register_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./register.page */ "./src/app/register/register.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _register_page__WEBPACK_IMPORTED_MODULE_5__["RegisterPage"]
    }
];
var RegisterPageModule = /** @class */ (function () {
    function RegisterPageModule() {
    }
    RegisterPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"]
            ],
            declarations: [_register_page__WEBPACK_IMPORTED_MODULE_5__["RegisterPage"]]
        })
    ], RegisterPageModule);
    return RegisterPageModule;
}());



/***/ }),

/***/ "./src/app/register/register.page.html":
/*!*********************************************!*\
  !*** ./src/app/register/register.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n        <ion-buttons slot=\"start\">\n            <ion-back-button color=\"energized\"></ion-back-button>\n        </ion-buttons>\n        <ion-title>\n            <h2>Register</h2>\n        </ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n    <ion-grid>\n\n\n        <form [formGroup]=\"registerForm\" (ngSubmit)=\"onSubmit()\">\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"3\">\n                    <img src=\"assets/images/camera.png\" width=\"100%\" alt=\"placeholder+image\">\n                    <!-- <input type=\"file\"> -->\n                </ion-col>\n                <ion-col size=\"7\" class=\"login-block\">\n                    name\n                    <input type=\"text\" formControlName=\"name\" autocomplete=\"off\">\n                    <div *ngIf=\"submitted && registerForm.controls.name.errors\" class=\"error\">\n                        <div style=\"color: red;  margin-top: -20px;  position: absolute;\"\n                             *ngIf=\"registerForm.controls.name.errors.required\">A name is required\n                        </div>\n                    </div>\n                </ion-col>\n                <ion-col></ion-col>\n            </ion-row>\n\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"10\" class=\"login-block\">\n                    Image\n                    <input type=\"file\" (change)=\"onFileSelected($event)\">\n                </ion-col>\n            </ion-row>\n\n\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"10\" class=\"login-block\">\n                    Email\n                    <input type=\"text\" autocapitalize=\"none\" formControlName=\"email\">\n                    <div *ngIf=\"submitted && registerForm.controls.email.errors\" class=\"error\">\n                        <div style=\"color: red;  margin-top: -20px;  position: absolute;\"\n                             *ngIf=\"registerForm.controls.email.errors.required\">An email is\n                            required\n                        </div>\n                        <div style=\"color: red;  margin-top: -20px;  position: absolute;\"\n                             *ngIf=\"registerForm.controls.email.errors.email\">Invalid email address\n                        </div>\n                    </div>\n                </ion-col>\n                <ion-col></ion-col>\n            </ion-row>\n\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"10\" class=\"login-block\">\n                    Password\n                    <input type=\"password\" formControlName=\"password\">\n                    <div *ngIf=\"submitted && registerForm.controls.password.errors\" class=\"error\">\n                        <div style=\"color: red;  margin-top: -20px;  position: absolute;\"\n                             *ngIf=\"registerForm.controls.password.errors.required\">A password is\n                            required\n                        </div>\n                        <div style=\"color: red;  margin-top: -20px;  position: absolute;\"\n                             *ngIf=\"registerForm.controls.password.errors.minlength\">Password must be\n                            at least 6 characters\n                        </div>\n                    </div>\n                </ion-col>\n                <ion-col></ion-col>\n            </ion-row>\n\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"10\" class=\"login-block\">\n                    Re-enter password\n                    <input type=\"password\" formControlName=\"confirmPassword\">\n                    <div *ngIf=\"submitted && registerForm.controls.confirmPassword.errors\" class=\"error\">\n                        <div style=\"color: red;  margin-top: -20px;  position: absolute;\"\n                             *ngIf=\"registerForm.controls.confirmPassword.errors.required\">Confirm\n                            Password is required\n                        </div>\n                        <div style=\"color: red;  margin-top: -20px;  position: absolute;\"\n                             *ngIf=\"registerForm.controls.confirmPassword.errors.mustMatch\">Passwords\n                            must match\n                        </div>\n                    </div>\n\n                </ion-col>\n                <ion-col></ion-col>\n            </ion-row>\n\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"10\" class=\"login-block\">\n                    <button ion-button\n                            full\n                            color=\"#fd6700\"\n                    >\n                        Register\n                    </button>\n                </ion-col>\n                <ion-col></ion-col>\n            </ion-row>\n        </form>\n    </ion-grid>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/register/register.page.scss":
/*!*********************************************!*\
  !*** ./src/app/register/register.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "label {\n  display: block; }\n  label input, label textarea {\n    display: block;\n    width: 50%;\n    margin-bottom: 20px;\n    padding: 1em; }\n  label .error {\n    margin-top: -20px;\n    background: #ff1e00;\n    padding: 0.5em;\n    display: inline-block;\n    font-size: 0.9em;\n    margin-bottom: 20px; }\n  .cta {\n  background: #ee801a;\n  border: none;\n  color: white;\n  text-transform: uppercase;\n  border-radius: 4px;\n  padding: 1em;\n  cursor: pointer;\n  font-family: 'Montserrat'; }\n  .results {\n  margin-top: 50px; }\n  .results strong {\n    display: block; }\n  .results span {\n    margin-bottom: 20px;\n    display: block; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYW5kaWthcHJhc2FkL0RvY3VtZW50cy9mcmVlbGFuY2Uvc2FtZWVyYS9ldmVudGV4cGVuc2VzL3NyYy9hcHAvcmVnaXN0ZXIvcmVnaXN0ZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBYyxFQWlCZjtFQWxCRDtJQUlJLGVBQWM7SUFDZCxXQUFVO0lBQ1Ysb0JBQW1CO0lBQ25CLGFBQVksRUFDYjtFQVJIO0lBV0ksa0JBQWlCO0lBQ2pCLG9CQUEyQjtJQUMzQixlQUFjO0lBQ2Qsc0JBQXFCO0lBQ3JCLGlCQUFnQjtJQUNoQixvQkFBbUIsRUFDcEI7RUFHSDtFQUNFLG9CQUE2QjtFQUM3QixhQUFZO0VBQ1osYUFBWTtFQUNaLDBCQUF5QjtFQUN6QixtQkFBa0I7RUFDbEIsYUFBWTtFQUNaLGdCQUFlO0VBQ2YsMEJBQXlCLEVBQzFCO0VBRUQ7RUFDRSxpQkFBZ0IsRUFVakI7RUFYRDtJQUlJLGVBQWMsRUFDZjtFQUxIO0lBUUksb0JBQW1CO0lBQ25CLGVBQWMsRUFDZiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdGVyL3JlZ2lzdGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG5cbiAgaW5wdXQsIHRleHRhcmVhIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICB3aWR0aDogNTAlO1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgcGFkZGluZzogMWVtO1xuICB9XG5cbiAgLmVycm9yIHtcbiAgICBtYXJnaW4tdG9wOiAtMjBweDtcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAzMCwgMCk7XG4gICAgcGFkZGluZzogMC41ZW07XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIGZvbnQtc2l6ZTogMC45ZW07XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgfVxufVxuXG4uY3RhIHtcbiAgYmFja2dyb3VuZDogcmdiKDIzOCwgMTI4LCAyNik7XG4gIGJvcmRlcjogbm9uZTtcbiAgY29sb3I6IHdoaXRlO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIHBhZGRpbmc6IDFlbTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQnO1xufVxuXG4ucmVzdWx0cyB7XG4gIG1hcmdpbi10b3A6IDUwcHg7XG5cbiAgc3Ryb25nIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuXG4gIHNwYW4ge1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/register/register.page.ts":
/*!*******************************************!*\
  !*** ./src/app/register/register.page.ts ***!
  \*******************************************/
/*! exports provided: RegisterPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPage", function() { return RegisterPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _helpers_must_match_validator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_helpers/must-match.validator */ "./src/app/_helpers/must-match.validator.ts");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _models_User__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../models/User */ "./src/app/models/User.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var RegisterPage = /** @class */ (function () {
    /**
     *
     * @param formBuilder
     * @param data
     * @param loadingController
     * @param router
     * @param authService
     */
    function RegisterPage(formBuilder, data, loadingController, router, authService) {
        this.formBuilder = formBuilder;
        this.data = data;
        this.loadingController = loadingController;
        this.router = router;
        this.authService = authService;
        this.submitted = false;
        this.success = false;
        this.selectedFile = null;
        this.registerForm = this.formBuilder.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6)]],
            confirmPassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        }, {
            validator: Object(_helpers_must_match_validator__WEBPACK_IMPORTED_MODULE_2__["MustMatch"])('password', 'confirmPassword')
        });
    }
    RegisterPage.prototype.ngOnInit = function () {
    };
    /**
     *
     * @param event
     */
    RegisterPage.prototype.onFileSelected = function (event) {
        this.selectedFile = event.target.files[0];
    };
    /**
     * On register form submit
     */
    RegisterPage.prototype.onSubmit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loading, user, names;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.submitted = true;
                        if (this.registerForm.invalid) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.loadingController.create({
                                message: 'Please wait...',
                                spinner: 'circles',
                            })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        user = new _models_User__WEBPACK_IMPORTED_MODULE_7__["User"]();
                        names = this.registerForm.value.name.split(' ');
                        user.firstName = names[0];
                        user.lastName = names.length > 1 ? names[1] : '.';
                        user.email = this.registerForm.value.email;
                        user.password = this.registerForm.value.password;
                        this.authService.registerUser(user).then(function (response) {
                            var data = JSON.parse(response.data);
                            if (data.success) {
                                loading.dismiss();
                                alert('Registration successful');
                                _this.router.navigate(['/login']);
                            }
                            else {
                                alert('Registration failed');
                                loading.dismiss();
                            }
                            console.log('response', response.data);
                        }, function (error) {
                            console.log('error', error);
                            alert('Registration failed');
                            loading.dismiss();
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.page.html */ "./src/app/register/register.page.html"),
            styles: [__webpack_require__(/*! ./register.page.scss */ "./src/app/register/register.page.scss")],
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]])
    ], RegisterPage);
    return RegisterPage;
}());



/***/ })

}]);
//# sourceMappingURL=register-register-module.js.map