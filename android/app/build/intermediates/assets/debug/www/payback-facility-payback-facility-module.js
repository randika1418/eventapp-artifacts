(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["payback-facility-payback-facility-module"],{

/***/ "./src/app/payback-facility/payback-facility.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/payback-facility/payback-facility.module.ts ***!
  \*************************************************************/
/*! exports provided: PaybackFacilityPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaybackFacilityPageModule", function() { return PaybackFacilityPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _payback_facility_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./payback-facility.page */ "./src/app/payback-facility/payback-facility.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _payback_facility_page__WEBPACK_IMPORTED_MODULE_5__["PaybackFacilityPage"]
    }
];
var PaybackFacilityPageModule = /** @class */ (function () {
    function PaybackFacilityPageModule() {
    }
    PaybackFacilityPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_payback_facility_page__WEBPACK_IMPORTED_MODULE_5__["PaybackFacilityPage"]]
        })
    ], PaybackFacilityPageModule);
    return PaybackFacilityPageModule;
}());



/***/ }),

/***/ "./src/app/payback-facility/payback-facility.page.html":
/*!*************************************************************!*\
  !*** ./src/app/payback-facility/payback-facility.page.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n        <ion-buttons slot=\"start\">\n            <ion-back-button color=\"energized\"></ion-back-button>\n        </ion-buttons>\n        <ion-title>\n            <h2>Payback Facility</h2>\n        </ion-title>\n        <!--        <ion-row>-->\n        <!--            <ion-col size=\"12\">-->\n        <!--                <h2>Payback Facility</h2>-->\n        <!--            </ion-col>-->\n        <!--        </ion-row>-->\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n    <ion-grid>\n        <ion-row>\n            <ion-col></ion-col>\n            <ion-col size=\"10\" class=\"login-block\">\n                Event name\n                <select>\n                    <option>Event 1</option>\n                    <option>Event 2</option>\n                    <option>Event 3</option>\n                </select>\n            </ion-col>\n            <ion-col></ion-col>\n        </ion-row>\n\n        <ion-row>\n            <ion-col></ion-col>\n            <ion-col size=\"10\" class=\"login-block\">\n                Amount (SGD)\n                <input type=\"text\" name=\"\">\n            </ion-col>\n            <ion-col></ion-col>\n        </ion-row>\n\n        <ion-row>\n            <ion-col></ion-col>\n            <ion-col size=\"10\" class=\"login-block\">\n                <button ion-button full color=\"#fd6700\">Pay</button>\n            </ion-col>\n            <ion-col></ion-col>\n        </ion-row>\n\n    </ion-grid>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/payback-facility/payback-facility.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/payback-facility/payback-facility.page.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BheWJhY2stZmFjaWxpdHkvcGF5YmFjay1mYWNpbGl0eS5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/payback-facility/payback-facility.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/payback-facility/payback-facility.page.ts ***!
  \***********************************************************/
/*! exports provided: PaybackFacilityPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaybackFacilityPage", function() { return PaybackFacilityPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PaybackFacilityPage = /** @class */ (function () {
    function PaybackFacilityPage() {
    }
    PaybackFacilityPage.prototype.ngOnInit = function () {
    };
    PaybackFacilityPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-payback-facility',
            template: __webpack_require__(/*! ./payback-facility.page.html */ "./src/app/payback-facility/payback-facility.page.html"),
            styles: [__webpack_require__(/*! ./payback-facility.page.scss */ "./src/app/payback-facility/payback-facility.page.scss")],
        }),
        __metadata("design:paramtypes", [])
    ], PaybackFacilityPage);
    return PaybackFacilityPage;
}());



/***/ })

}]);
//# sourceMappingURL=payback-facility-payback-facility-module.js.map