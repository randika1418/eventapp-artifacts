(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_5__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"]
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_5__["LoginPage"]],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/login/login.page.html":
/*!***************************************!*\
  !*** ./src/app/login/login.page.html ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n    <ion-grid>\n        <form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\" #loginFormDir=\"ngForm\">\n            <ion-row>\n                <ion-col size=\"3\"></ion-col>\n                <ion-col size=\"6\" class=\"logo\">\n                    <img src=\"assets/images/logo.png\" width=\"80%\" alt=\"placeholder+image\"/>\n                </ion-col>\n                <ion-col size=\"3\"></ion-col>\n            </ion-row>\n\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"10\" class=\"login-block\">\n                    Email\n                    <input autocapitalize=\"none\" type=\"text\" formControlName=\"email\"/>\n                    <div *ngIf=\"submitted && loginForm.controls.email.errors\" class=\"error\">\n                        <div style=\"color: red;  margin-top: -20px;  position: absolute;\"\n                             *ngIf=\"loginForm.controls.email.errors.required\">Email is required\n                        </div>\n                    </div>\n                </ion-col>\n                <ion-col></ion-col>\n            </ion-row>\n\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"10\" class=\"login-block\">\n                    Password\n                    <input type=\"password\" formControlName=\"password\"/>\n                    <div *ngIf=\"submitted && loginForm.controls.password.errors\" class=\"error\">\n                        <div style=\"color: red;  margin-top: -20px;  position: absolute;\"\n                             *ngIf=\"loginForm.controls.password.errors.required\">Password is required\n                        </div>\n                    </div>\n                </ion-col>\n                <ion-col></ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col></ion-col>\n                <ion-col size=\"10\" class=\"login-block\">\n                    <button ion-button\n                            full\n                            color=\"#fd6700\"\n                    >\n                        Login\n                    </button>\n                </ion-col>\n                <ion-col></ion-col>\n            </ion-row>\n\n            <ion-row>\n                <ion-col size=\"12\" class=\"login-footer\">\n                    <span [routerLink]=\"['/register']\">Register</span> &nbsp; &nbsp; &nbsp;\n                    Forgot password\n                </ion-col>\n            </ion-row>\n\n            <ion-row>\n                <ion-col size=\"12\" class=\"login-footer\">\n                    Login with\n                    <span style=\"color:blue\" (click)=\"doFbLogin($event)\">Facebook</span>\n                </ion-col>\n            </ion-row>\n        </form>\n    </ion-grid>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content.login-content {\n  --background: url('splash.png') 0 0/100% 100% no-repeat; }\n\n.logo {\n  color: white;\n  font-size: 64px;\n  font-weight: bold;\n  text-align: center;\n  padding-top: 50px; }\n\n.description {\n  color: white;\n  font-size: 22px;\n  text-align: center; }\n\n.log-in-button {\n  margin-top: 500px;\n  font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yYW5kaWthcHJhc2FkL0RvY3VtZW50cy9mcmVlbGFuY2Uvc2FtZWVyYS9ldmVudGV4cGVuc2VzL3NyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0Usd0RBQWEsRUFDZDs7QUFFRDtFQUNFLGFBQVk7RUFDWixnQkFBZTtFQUNmLGtCQUFpQjtFQUNqQixtQkFBa0I7RUFDbEIsa0JBQWlCLEVBQ2xCOztBQUVEO0VBQ0UsYUFBWTtFQUNaLGdCQUFlO0VBQ2YsbUJBQWtCLEVBQ25COztBQUVEO0VBQ0Usa0JBQWlCO0VBQ2pCLGtCQUFpQixFQUNsQiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW9uLWNvbnRlbnQubG9naW4tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdXJsKCcuLi8uLi9hc3NldHMvc3BsYXNoLnBuZycpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xufVxuXG4ubG9nbyB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiA2NHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLXRvcDogNTBweDtcbn1cblxuLmRlc2NyaXB0aW9uIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDIycHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmxvZy1pbi1idXR0b24ge1xuICBtYXJnaW4tdG9wOiA1MDBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "./node_modules/@ionic-native/facebook/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_native_storage_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/native-storage/ngx */ "./node_modules/@ionic-native/native-storage/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _models_User__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../models/User */ "./src/app/models/User.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../config */ "./src/app/config/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};












var LoginPage = /** @class */ (function () {
    /**
     *
     * @param fb
     * @param nativeStorage
     * @param loadingController
     * @param router
     * @param platform
     * @param alertController
     * @param menuCtrl
     * @param data
     * @param authService
     * @param storage
     * @param formBuilder
     * @param config
     */
    function LoginPage(fb, nativeStorage, loadingController, router, platform, alertController, menuCtrl, data, authService, storage, formBuilder, config) {
        this.fb = fb;
        this.nativeStorage = nativeStorage;
        this.loadingController = loadingController;
        this.router = router;
        this.platform = platform;
        this.alertController = alertController;
        this.menuCtrl = menuCtrl;
        this.data = data;
        this.authService = authService;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.config = config;
        this.user = new _models_User__WEBPACK_IMPORTED_MODULE_7__["User"]();
        this.loginForm = this.formBuilder.group({
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].email]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(6)]],
        });
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    LoginPage.prototype.onSubmit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var user;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.submitted = true;
                        if (this.loginForm.invalid) {
                            return [2 /*return*/];
                        }
                        user = new _models_User__WEBPACK_IMPORTED_MODULE_7__["User"]();
                        user.email = this.loginForm.value.email;
                        user.password = this.loginForm.value.password;
                        return [4 /*yield*/, this.doDefaultLogin(user)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Default user login with email and password.
     * @param user the user object.
     */
    LoginPage.prototype.doDefaultLogin = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            spinner: 'circles',
                            message: 'Please wait...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        this.authService.authenticateByUserNameAndPassword(user).then(function (response) { return __awaiter(_this, void 0, void 0, function () {
                            var data, e_1;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (!(response.data && response.status === 200)) return [3 /*break*/, 5];
                                        this.resetForm();
                                        data = JSON.parse(response.data);
                                        _a.label = 1;
                                    case 1:
                                        _a.trys.push([1, 3, , 4]);
                                        return [4 /*yield*/, this.nativeStorage.setItem('token', { data: data.access_token })];
                                    case 2:
                                        _a.sent();
                                        this.router.navigate(['/home']);
                                        loading.dismiss();
                                        return [3 /*break*/, 4];
                                    case 3:
                                        e_1 = _a.sent();
                                        alert('Error, Please try again');
                                        loading.dismiss();
                                        return [3 /*break*/, 4];
                                    case 4: return [3 /*break*/, 6];
                                    case 5:
                                        alert('Login failed');
                                        loading.dismiss();
                                        _a.label = 6;
                                    case 6: return [2 /*return*/];
                                }
                            });
                        }); }, function (error) {
                            alert('Login failed');
                            loading.dismiss();
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Present loading screen.
     * @param loading
     */
    LoginPage.prototype.presentLoading = function (loading) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LoginPage.prototype.doFbLogin = function ($event) {
        return __awaiter(this, void 0, void 0, function () {
            var loading, permissions;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            spinner: 'circles',
                            message: 'Please wait...'
                        })];
                    case 1:
                        loading = _a.sent();
                        this.presentLoading(loading);
                        permissions = ['public_profile', 'email'];
                        this.fb.login(permissions).then(function (response) { return __awaiter(_this, void 0, void 0, function () {
                            var userId;
                            var _this = this;
                            return __generator(this, function (_a) {
                                userId = response.authResponse.userID;
                                if (response.authResponse.accessToken) {
                                    console.log('FB TOKEN: ', response.authResponse.accessToken);
                                    // await this.router.navigate(['/home']);
                                    this.authService.registerByFbAccessToken(response.authResponse.accessToken).then(function (apiResponse) {
                                        _this.loginUsingFbToken(response.authResponse.accessToken, loading);
                                    }, function (error) {
                                        if (error.status === 422) {
                                            _this.loginUsingFbToken(response.authResponse.accessToken, loading);
                                        }
                                    });
                                }
                                return [2 /*return*/];
                            });
                        }); }, function (error) {
                            console.log(error);
                            loading.dismiss();
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.loginUsingFbToken = function (token, loading) {
        var _this = this;
        this.authService.loginViaFbToken(token).then(function (apiResponse) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        loading.dismiss();
                        this.resetForm();
                        return [4 /*yield*/, this.nativeStorage.setItem('token', { data: JSON.parse(apiResponse.data).access_token })];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.router.navigate(['/home'])];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); }, function (error) {
            loading.dismiss();
            alert('Login failed');
        });
    };
    LoginPage.prototype.resetForm = function () {
        this.loginFormDir.resetForm();
        this.submitted = false;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('loginFormDir'),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgForm"])
    ], LoginPage.prototype, "loginFormDir", void 0);
    LoginPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.page.html */ "./src/app/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_1__["Facebook"],
            _ionic_native_native_storage_ngx__WEBPACK_IMPORTED_MODULE_3__["NativeStorage"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"],
            _data_service__WEBPACK_IMPORTED_MODULE_6__["DataService"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_8__["AuthService"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["Storage"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            _config__WEBPACK_IMPORTED_MODULE_10__["Config"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ })

}]);
//# sourceMappingURL=login-login-module.js.map