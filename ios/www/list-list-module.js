(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["list-list-module"],{

/***/ "./src/app/list/list.module.ts":
/*!*************************************!*\
  !*** ./src/app/list/list.module.ts ***!
  \*************************************/
/*! exports provided: ListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListPageModule", function() { return ListPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _list_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list.page */ "./src/app/list/list.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _list_page__WEBPACK_IMPORTED_MODULE_5__["ListPage"]
    }
];
var ListPageModule = /** @class */ (function () {
    function ListPageModule() {
    }
    ListPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_list_page__WEBPACK_IMPORTED_MODULE_5__["ListPage"]]
        })
    ], ListPageModule);
    return ListPageModule;
}());



/***/ }),

/***/ "./src/app/list/list.page.html":
/*!*************************************!*\
  !*** ./src/app/list/list.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <!-- <ion-toolbar>\n      <ion-title>\n        Ionic Blank\n      </ion-title>\n    </ion-toolbar> -->\n\n    <ion-row class=\"home-header\">\n        <ion-col size=\"1\"><img src=\"assets/images/menu.png\" width=\"80%\" alt=\"placeholder+image\"></ion-col>\n        <ion-col size=\"10\"><img src=\"assets/images/top_logo.png\" width=\"\" height=\"30px\" alt=\"placeholder+image\">\n        </ion-col>\n        <ion-col size=\"1\"><img src=\"assets/images/messages.png\" width=\"80%\" alt=\"placeholder+image\"></ion-col>\n    </ion-row>\n\n</ion-header>\n\n\n<ion-content padding>\n    <ion-list>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Jhon Doe</h2>\n            <br>\n            <p>Jhon@gmail.com</p>\n        </ion-item>\n        <ion-item inset=\"true\">\n            <ion-avatar item-start>\n                <img src=\"../../assets/avatar.png\">\n            </ion-avatar>\n            <h2>Lorium Epsume</h2>\n            <br>\n            <p>Lorium@gmail.com</p>\n        </ion-item>\n    </ion-list>\n    <!-- <ion-list>\n      <ion-item>\n        <ion-label>Pokémon Yellow</ion-label>\n      </ion-item>\n      <ion-item>\n        <ion-label>Mega Man X</ion-label>\n      </ion-item>\n      <ion-item>\n        <ion-label>The Legend of Zelda</ion-label>\n      </ion-item>\n      <ion-item>\n        <ion-label>Pac-Man</ion-label>\n      </ion-item>\n      <ion-item>\n        <ion-label>Super Mario World</ion-label>\n      </ion-item>\n    </ion-list> -->\n</ion-content>\n"

/***/ }),

/***/ "./src/app/list/list.page.scss":
/*!*************************************!*\
  !*** ./src/app/list/list.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xpc3QvbGlzdC5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/list/list.page.ts":
/*!***********************************!*\
  !*** ./src/app/list/list.page.ts ***!
  \***********************************/
/*! exports provided: ListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListPage", function() { return ListPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ListPage = /** @class */ (function () {
    function ListPage() {
    }
    ListPage.prototype.ngOnInit = function () {
    };
    ListPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.page.html */ "./src/app/list/list.page.html"),
            styles: [__webpack_require__(/*! ./list.page.scss */ "./src/app/list/list.page.scss")],
        }),
        __metadata("design:paramtypes", [])
    ], ListPage);
    return ListPage;
}());



/***/ })

}]);
//# sourceMappingURL=list-list-module.js.map