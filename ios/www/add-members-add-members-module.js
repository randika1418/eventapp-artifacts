(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-members-add-members-module"],{

/***/ "./src/app/add-members/add-members.module.ts":
/*!***************************************************!*\
  !*** ./src/app/add-members/add-members.module.ts ***!
  \***************************************************/
/*! exports provided: AddMembersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddMembersPageModule", function() { return AddMembersPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _add_members_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-members.page */ "./src/app/add-members/add-members.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _add_members_page__WEBPACK_IMPORTED_MODULE_5__["AddMembersPage"]
    }
];
var AddMembersPageModule = /** @class */ (function () {
    function AddMembersPageModule() {
    }
    AddMembersPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"]
            ],
            declarations: [_add_members_page__WEBPACK_IMPORTED_MODULE_5__["AddMembersPage"]]
        })
    ], AddMembersPageModule);
    return AddMembersPageModule;
}());



/***/ }),

/***/ "./src/app/add-members/add-members.page.html":
/*!***************************************************!*\
  !*** ./src/app/add-members/add-members.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n        <ion-buttons slot=\"start\">\n            <ion-back-button color=\"energized\"></ion-back-button>\n        </ion-buttons>\n        <ion-row>\n            <ion-col size=\"12\">\n                <h2>Add Members</h2>\n            </ion-col>\n        </ion-row>\n    </ion-toolbar>\n\n</ion-header>\n\n<ion-content>\n\n    <ion-grid>\n\n\n        <ion-row>\n            <ion-col size=\"12\">\n                <ion-searchbar></ion-searchbar>\n            </ion-col>\n        </ion-row>\n\n\n        <ion-row class=\"members-block\">\n            <ion-col size=\"3\">\n                <img src=\"assets/images/profile.jpg\" width=\"80%\" alt=\"placeholder+image\">\n            </ion-col>\n            <ion-col size=\"7\">\n                <h5>John Doe</h5>\n                <p>Johndoe@email.com</p>\n            </ion-col>\n            <ion-col size=\"1\">\n                <img src=\"assets/images/checked.png\" width=\"100%\" alt=\"placeholder+image\" style=\"border-radius: 0px;\">\n            </ion-col>\n            <ion-col></ion-col>\n        </ion-row>\n\n        <ion-row class=\"members-block\">\n            <ion-col size=\"3\">\n                <img src=\"assets/images/profile.jpg\" width=\"80%\" alt=\"placeholder+image\">\n            </ion-col>\n            <ion-col size=\"7\">\n                <h5>John Doe</h5>\n                <p>Johndoe@email.com</p>\n            </ion-col>\n            <ion-col size=\"1\">\n                <img src=\"assets/images/checked.png\" width=\"100%\" alt=\"placeholder+image\" style=\"border-radius: 0px;\">\n            </ion-col>\n            <ion-col></ion-col>\n        </ion-row>\n\n        <ion-row class=\"members-block\">\n            <ion-col size=\"3\">\n                <img src=\"assets/images/profile.jpg\" width=\"80%\" alt=\"placeholder+image\">\n            </ion-col>\n            <ion-col size=\"7\">\n                <h5>John Doe</h5>\n                <p>Johndoe@email.com</p>\n            </ion-col>\n            <ion-col size=\"1\">\n                <img src=\"assets/images/checked.png\" width=\"100%\" alt=\"placeholder+image\" style=\"border-radius: 0px;\">\n            </ion-col>\n            <ion-col></ion-col>\n        </ion-row>\n\n        <ion-row class=\"members-block\">\n            <ion-col size=\"3\">\n                <img src=\"assets/images/profile.jpg\" width=\"80%\" alt=\"placeholder+image\">\n            </ion-col>\n            <ion-col size=\"7\">\n                <h5>John Doe</h5>\n                <p>Johndoe@email.com</p>\n            </ion-col>\n            <ion-col size=\"1\">\n                <img src=\"assets/images/checked.png\" width=\"100%\" alt=\"placeholder+image\" style=\"border-radius: 0px;\">\n            </ion-col>\n            <ion-col></ion-col>\n        </ion-row>\n\n        <ion-row class=\"members-block\">\n            <ion-col size=\"3\">\n                <img src=\"assets/images/profile.jpg\" width=\"80%\" alt=\"placeholder+image\">\n            </ion-col>\n            <ion-col size=\"7\">\n                <h5>John Doe</h5>\n                <p>Johndoe@email.com</p>\n            </ion-col>\n            <ion-col size=\"1\">\n                <img src=\"assets/images/checked.png\" width=\"100%\" alt=\"placeholder+image\" style=\"border-radius: 0px;\">\n            </ion-col>\n            <ion-col></ion-col>\n        </ion-row>\n\n        <ion-row class=\"members-block\">\n            <ion-col size=\"3\">\n                <img src=\"assets/images/profile.jpg\" width=\"80%\" alt=\"placeholder+image\">\n            </ion-col>\n            <ion-col size=\"7\">\n                <h5>John Doe</h5>\n                <p>Johndoe@email.com</p>\n            </ion-col>\n            <ion-col size=\"1\">\n                <img src=\"assets/images/checked.png\" width=\"100%\" alt=\"placeholder+image\" style=\"border-radius: 0px;\">\n            </ion-col>\n            <ion-col></ion-col>\n        </ion-row>\n\n        <ion-row class=\"members-block\">\n            <ion-col size=\"3\">\n                <img src=\"assets/images/profile.jpg\" width=\"80%\" alt=\"placeholder+image\">\n            </ion-col>\n            <ion-col size=\"7\">\n                <h5>John Doe</h5>\n                <p>Johndoe@email.com</p>\n            </ion-col>\n            <ion-col size=\"1\">\n                <img src=\"assets/images/checked.png\" width=\"100%\" alt=\"placeholder+image\" style=\"border-radius: 0px;\">\n            </ion-col>\n            <ion-col></ion-col>\n        </ion-row>\n\n        <ion-row class=\"members-block\">\n            <ion-col size=\"3\">\n                <img src=\"assets/images/profile.jpg\" width=\"80%\" alt=\"placeholder+image\">\n            </ion-col>\n            <ion-col size=\"7\">\n                <h5>John Doe</h5>\n                <p>Johndoe@email.com</p>\n            </ion-col>\n            <ion-col size=\"1\">\n                <img src=\"assets/images/checked.png\" width=\"100%\" alt=\"placeholder+image\" style=\"border-radius: 0px;\">\n            </ion-col>\n            <ion-col></ion-col>\n        </ion-row>\n\n        <ion-row class=\"add-block\">\n            <ion-col></ion-col>\n            <ion-col size=\"10\" class=\"login-block\">\n                <button ion-button full color=\"#fd6700\">Add</button>\n            </ion-col>\n            <ion-col></ion-col>\n        </ion-row>\n\n    </ion-grid>\n\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/add-members/add-members.page.scss":
/*!***************************************************!*\
  !*** ./src/app/add-members/add-members.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkZC1tZW1iZXJzL2FkZC1tZW1iZXJzLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/add-members/add-members.page.ts":
/*!*************************************************!*\
  !*** ./src/app/add-members/add-members.page.ts ***!
  \*************************************************/
/*! exports provided: AddMembersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddMembersPage", function() { return AddMembersPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AddMembersPage = /** @class */ (function () {
    function AddMembersPage(formBuilder) {
        this.formBuilder = formBuilder;
        this.submitted = false;
        this.success = false;
        this.messageForm = this.formBuilder.group({
            first_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            last_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
    }
    AddMembersPage.prototype.ngOnInit = function () {
    };
    AddMembersPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-members',
            template: __webpack_require__(/*! ./add-members.page.html */ "./src/app/add-members/add-members.page.html"),
            styles: [__webpack_require__(/*! ./add-members.page.scss */ "./src/app/add-members/add-members.page.scss")],
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], AddMembersPage);
    return AddMembersPage;
}());



/***/ })

}]);
//# sourceMappingURL=add-members-add-members-module.js.map